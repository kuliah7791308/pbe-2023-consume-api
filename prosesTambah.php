<?php
require_once 'vendor/autoload.php';

use GuzzleHttp\Client;

$header = ['pbe_token' => '1ee55c09-6827-66e2-b831-98fa9ba4c8a9'];
$client = new Client(['base_uri' => 'http://127.0.0.1:8000/api/']);

$bodyRequest = [
    'code' => $_REQUEST['code'],
    'title' => $_REQUEST['title'],
    'published_year' => $_REQUEST['published_year'],
    'city' => $_REQUEST['city'],
    'id_publisher' => $_REQUEST['id_publisher'],
];

$response = $client->request('POST', 'books', [
    'headers' => $header,
    'json' => $bodyRequest
]);

header('Location: index.php');
