<?php
require_once 'vendor/autoload.php';

use GuzzleHttp\Client;

$header = ['pbe_token' => '1ee55c09-6827-66e2-b831-98fa9ba4c8a9'];
$client = new Client(['base_uri' => 'http://127.0.0.1:8000/api/']);
$response = $client->request('GET', 'publishers', [
    'headers' => $header
]);
$body = json_decode($response->getBody(), true);
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <div class="container">
        <form method="POST" action="prosesTambah.php">
            <div class="form-group">
                <label for="">Kode</label>
                <input type="text" name="code" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Judul</label>
                <input type="text" name="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Tahun Terbit</label>
                <input type="number" min='1900' max='2023' name="published_year" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Kota</label>
                <input type="text" name="city" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Publisher</label>
                <select name="id_publisher" class="form-control" id="">
                    <?php 
                    foreach ($body['data'] as $p) {
                        $id = $p['id'];
                        $label = $p['name'];
                        echo "<option value='$id'>$label</option>";
                    }
                    ?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</body>

</html>