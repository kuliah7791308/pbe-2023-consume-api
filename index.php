<?php
require_once 'vendor/autoload.php';

use GuzzleHttp\Client;

$header = ['pbe_token' => '1ee55c09-6827-66e2-b831-98fa9ba4c8a9'];
$client = new Client(['base_uri' => 'http://127.0.0.1:8000/api/']);
$response = $client->request('GET', 'books', [
    'headers' => $header
]);
$body = json_decode($response->getBody(), true);
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Hello, world!</title>
</head>

<body>
    <div class="container">
        <table class="table">
            <thead>
                <th>No</th>
                <th>Kode</th>
                <th>Judul</th>
                <th>Tahun Terbit</th>
                <th>Kota</th>
                <th>Aksi</th>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($body['data'] as $row) {
                ?>
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $row['code'] ?></td>
                        <td><?= $row['title'] ?></td>
                        <td><?= $row['published_year'] ?></td>
                        <td><?= $row['city'] ?></td>
                        <td></td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</body>

</html>